from django.shortcuts import render, redirect
from django.core import serializers
from django.http import Http404
from django.contrib.auth.decorators import login_required

from .forms import RegistrationForm
from .models import Item, Cart, Order, OrderCart, User

def index(request):
    return render(request, 'shop/index.html')

def catalog(request):
    try:
        items = Item.objects.all()
    except Item.DoesNotExist:
        raise Http404("Таблица каталога пуста")
    return render(request, 'shop/catalog.html', {'items': items})

def detail(request, id_item):
    try:
        item = Item.objects.get(pk=id_item)
    except Item.DoesNotExist:
        raise Http404("Таблица каталога пуста")
    return render(request, 'shop/detail.html', {'item': item})

def silk(request):
    return render(request, 'shop/silk.html')

def registration(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/accounts/login/")
    else:
        form = RegistrationForm()

    return render(request, "shop/register.html", {"form":form})

@login_required
def cart(request):
    try:
        carts = Cart.objects.filter(user=request.user)
    except Cart.DoesNotExist:  
        raise Http404("Корзина у пользователя пуста")  
    total = 0
    for cart in carts:
        total = total + cart.value()
    print(request.user.is_regular)
    if request.user.is_regular:
        total = float(total) - float(total)*0.02

    return render(request, 'shop/cart.html', {'carts': carts, 'total': total})

@login_required
def add_cart(request, id_cart):
    cart_obj = Cart.objects.get(pk=id_cart)
    cart_obj.amount += 1
    cart_obj.save()
    return redirect("/cart/")

@login_required
def remove_cart(request, id_cart):
    cart_obj = Cart.objects.get(pk=id_cart)
    if cart_obj.amount > 1:
        cart_obj.amount -= 1
        cart_obj.save()
    else:
        cart_obj.delete()
    return redirect("/cart/")

@login_required
def delete_cart(request, id_cart):
    cart_obj = Cart.objects.get(pk=id_cart)
    cart_obj.delete()
    return redirect("/cart/")

@login_required
def add_to_cart(request):
    if request.method == "POST":
        item_id = request.POST.get("item_id")
        item = Item.objects.get(pk=item_id)
        user = request.user
        obj, created = Cart.objects.get_or_create(item=item, user=user)
        if not created:
            obj.amount += 1
            obj.save()
    items = Item.objects.all()
    return render(request, 'shop/catalog.html', {'items': items})

@login_required
def create_order(request):
    try:
        carts = Cart.objects.filter(user=request.user)
    except Cart.DoesNotExist:  
        raise Http404("Корзина у пользователя пуста")

    order = Order(user=request.user)
    order.save()

    total = 0
    for cart in carts:
        total = total + cart.value()
        ordercart = OrderCart(order=order, name=cart.item.name, color=cart.item.color, value=cart.value(), amount=cart.amount)
        ordercart.save()
        cart.delete()

    if request.user.is_regular:
        total = float(total) - float(total)*0.02
    order.total = total
    order.save()

    request.user.set_regular()

    return redirect("/order_list/")

@login_required
def order_list(request):
    try:
        orders = Order.objects.filter(user=request.user)
    except Order.DoesNotExist:  
        raise Http404("У пользователя нет заказов")
    return render(request, 'shop/order_list.html', {'orders': orders, 'user':request.user})

@login_required
def order_detail(request, id_order):
    try:
        order = Order.objects.get(pk=id_order)
        ordercarts = OrderCart.objects.filter(order=order)
    except Order.DoesNotExist:  
        raise Http404("У пользователя нет заказа с таким номером")
    except OrderCart.DoesNotExist:  
        raise Http404("У пользователя нет заказа с таким номером")
    total = 0
    for ordercart in ordercarts:
        total = total + ordercart.value
    return render(request, 'shop/order_detail.html', {'ordercarts': ordercarts, 'total': total})

@login_required
def profile(request):
    return render(request, 'registration/profile.html')