from django.urls import path
from . import views
from django.conf.urls import url


urlpatterns = [
    path('', views.index, name='index'),
    path('shop/', views.index, name='index'),
    path('silk/', views.silk, name='silk'),
    path('catalog/', views.catalog, name='catalog'),
    path('detail/<int:id_item>', views.detail, name='detail'),
    path("registration/", views.registration, name="registration"),
    path("add_to_cart/", views.add_to_cart, name="add_to_cart"),
    path("cart/", views.cart, name="cart"),
    path("cart/add_cart/<int:id_cart>", views.add_cart, name="add_cart"),
    path("cart/remove_cart/<int:id_cart>", views.remove_cart, name="remove_cart"),
    path("cart/delete_cart/<int:id_cart>", views.delete_cart, name="delete_cart"),
    path("create_order/", views.create_order, name="create_order"),
    path("order_list/", views.order_list, name="order_list"),
    path("order_detail/<int:id_order>", views.order_detail, name="order_detail"),
    path("profile/", views.profile, name="profile"),
]
