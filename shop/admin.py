from django.contrib import admin

from .models import Item, Order, Cart, OrderCart, User

admin.site.register(Item)
admin.site.register(Cart)
admin.site.register(Order)
admin.site.register(OrderCart)
admin.site.register(User)